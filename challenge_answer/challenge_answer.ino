#define DATA_PIN 2
#define STORE_CLOCK 3
#define SHIFT_CLOCK 4

void pulse(int pinNum);

void setup() {
  pinMode(DATA_PIN, OUTPUT);
  pinMode(STORE_CLOCK, OUTPUT);
  pinMode(SHIFT_CLOCK, OUTPUT);

  digitalWrite(DATA_PIN, LOW);
  pulse(SHIFT_CLOCK);
  pulse(SHIFT_CLOCK);
  digitalWrite(DATA_PIN, HIGH);
  pulse(SHIFT_CLOCK);
  pulse(SHIFT_CLOCK);
  digitalWrite(DATA_PIN, LOW);
  pulse(SHIFT_CLOCK);
  pulse(SHIFT_CLOCK);
  digitalWrite(DATA_PIN, HIGH);
  pulse(SHIFT_CLOCK);
  pulse(SHIFT_CLOCK);
  pulse(STORE_CLOCK);
}

void loop() {
  digitalWrite(DATA_PIN, LOW);
  pulse(SHIFT_CLOCK);
  pulse(SHIFT_CLOCK);
  pulse(STORE_CLOCK);

  delay(250);

  digitalWrite(DATA_PIN, HIGH);
  pulse(SHIFT_CLOCK);
  pulse(SHIFT_CLOCK);
  pulse(STORE_CLOCK);

  delay(250);
}

void pulse(int pinNum) {
  digitalWrite(pinNum, HIGH);
  delayMicroseconds(10);
  digitalWrite(pinNum, LOW);
  delayMicroseconds(10);
}

